const joi = require("@hapi/joi")

const setArg = (arg, def = null) => {
	return arg == undefined ? def : arg
}

// Validation Type Object
const Type = {}

// Key
Type.key = function (args = {}) {
	req = setArg(args.req, true)
	def = setArg(args.def, null)
	positive = setArg(args.positive, true)

	// Default
	let result = joi.number().integer()

	// Is Positive
	if (positive) {
		result = result.positive()
	}

	// Required and Default
	result = req ? result.required() : result.allow('', null).empty(['', null]).default(def)

	// Returning
	return result
}

// Boolean
Type.boolean = function (args = {}) {
	req = setArg(args.req, true)
	def = setArg(args.def, null)

	// Default
	let result = joi.boolean()

	// Required and Default
	result = req ? result.required() : result.allow('', null).empty(['', null]).default(def)

	// Returning
	return result
}

// String
Type.string = function (args = {}) {
	req = setArg(args.req, true)
	def = setArg(args.def, null)
	trim = setArg(args.trim, true)
	length = setArg(parseInt(args.length), null)
	min = setArg(parseInt(args.min), null)
	max = setArg(parseInt(args.max), null)
	lowercase = setArg(args.lowercase, null)
	uppercase = setArg(args.uppercase, null)
	regex = setArg(args.regex, null)

	// Default
	let result = joi.string()

	// Trimming
	if (trim) {
		result = result.trim()
	}

	// Casing
	if (lowercase) {
		result = result.lowercase()
	}
	if (uppercase) {
		result = result.uppercase()
	}

	// Fixed Length
	if (length) {
		result = result.length(length)
	}
	else {
		// Minimum Length
		if (min) {
			result = result.min(min)
		}

		// Maximum Length
		if (max) {
			result = result.max(max)
		}
	}

	// Regex
	if (regex) {
		result = result.regex(regex)
	}

	// Required and Default
	result = req ? result.required() : result.allow('', null).empty(['', null]).default(def)

	// Returning
	return result
}

// Email
Type.email = function (args = {}) {
	req = setArg(args.req, true)
	def = setArg(args.def, null)
	length = setArg(parseInt(args.length), null)
	min = setArg(parseInt(args.min), null)
	max = setArg(parseInt(args.max), null)
	lowercase = setArg(args.lowercase, null)
	uppercase = setArg(args.uppercase, null)

	// Default
	let result = joi.string().trim().email()

	// Casing
	if (lowercase) {
		result = result.lowercase()
	}
	if (uppercase) {
		result = result.uppercase()
	}

	// Fixed Length
	if (length) {
		result = result.length(length)
	}
	else {
		// Minimum Length
		if (min) {
			result = result.min(min)
		}

		// Maximum Length
		if (max) {
			result = result.max(max)
		}
	}

	// Required and Default
	result = req ? result.required() : result.allow('', null).empty(['', null]).default(def)

	// Returning
	return result
}

// Integer
Type.integer = function (args = {}) {
	req = setArg(args.req, true)
	def = setArg(args.def, null)
	positive = setArg(args.positive, false)
	negative = setArg(args.negative, false)
	greater = setArg(args.greater, null)
	lesser = setArg(args.lesser, null)
	max = setArg(args.max, null)
	min = setArg(args.min, null)

	// Default
	let result = joi.number().integer()

	// Positive and Negative
	if (positive) {
		result = result.positive()
	}
	else if (negative) {
		result = result.negative()
	}

	// Greater
	if (greater) {
		result = result.greater(greater)
	}

	// Lesser
	if (lesser) {
		result = result.less(lesser)
	}

	// Max
	if (max) {
		result = result.max(max)
	}

	// Min
	if (min) {
		result = result.min(min)
	}

	// Required and Default
	result = req ? result.required() : result.allow('', null).empty(['', null]).default(def)

	// Returning
	return result
}

// Float
Type.float = function (args = {}) {
	req = setArg(args.req, true)
	def = setArg(args.def, null)
	positive = setArg(args.positive, false)
	negative = setArg(args.negative, false)
	greater = setArg(args.greater, null)
	lesser = setArg(args.lesser, null)
	max = setArg(args.max, null)
	min = setArg(args.min, null)
	precision = setArg(args.precision, null)

	// Default
	let result = joi.number()

	// Positive and Negative
	if (positive) {
		result = result.positive()
	}
	else if (negative) {
		result = result.negative()
	}

	// Greater
	if (greater) {
		result = result.greater(greater)
	}

	// Lesser
	if (lesser) {
		result = result.less(lesser)
	}

	// Max
	if (max) {
		result = result.max(max)
	}

	// Min
	if (min) {
		result = result.min(min)
	}

	// Precision
	if (precision) {
		result = result.precision(precision)
	}

	// Required and Default
	result = req ? result.required() : result.allow('', null).empty(['', null]).default(def)

	// Returning
	return result
}

// Date
Type.date = function (args = {}) {
	req = setArg(args.req, true)
	def = setArg(args.def, null)
	iso = setArg(args.iso, null)
	greater = setArg(args.greater, null)
	lesser = setArg(args.lesser, null)
	max = setArg(args.max, null)
	min = setArg(args.min, null)
	timestamp = setArg(args.timestamp, null)

	// Default
	let result = joi.date()

	// Positive and Negative
	if (iso) {
		result = result.iso()
	}

	// Greater
	if (greater) {
		result = result.greater(greater)
	}

	// Lesser
	if (lesser) {
		result = result.less(lesser)
	}

	// Max
	if (max) {
		result = result.max(max)
	}

	// Min
	if (min) {
		result = result.min(min)
	}

	// Timestamp
	if (timestamp) {
		if (timestamp == "default") {
			timestamp = ""
		}
		result = result.timestamp(timestamp)
	}

	// Required and Default
	result = req ? result.required() : result.allow('', null).empty(['', null]).default(def)

	// Returning
	return result
}

// Binary
Type.binary = function (args = {}) {
	req = setArg(args.req, true)
	def = setArg(args.def, null)
	encoding = setArg(args.encoding, null)
	length = setArg(args.length, null)
	max = setArg(args.max, null)
	min = setArg(args.min, null)

	// Default
	let result = joi.binary()

	// Encoding
	if (encoding) {
		result = result.encoding(encoding)
	}

	// Fixed Length
	if (length) {
		result = result.length(length)
	}
	else {
		// Minimum Length
		if (min) {
			result = result.min(min)
		}

		// Maximum Length
		if (max) {
			result = result.max(max)
		}
	}

	// Required and Default
	result = req ? result.required() : result.allow('', null).empty(['', null]).default(def)

	// Returning
	return result
}

// Array
Type.array = function (args = {}, schema) {
	req = setArg(args.req, true)
	def = setArg(args.def, null)
	length = setArg(args.length, null)
	max = setArg(args.max, null)
	min = setArg(args.min, null)
	has = setArg(args.has, null)
	ordered = setArg(args.ordered, null)
	sparse = setArg(args.sparse, false)

	// Default
	let result = joi.array().items(schema)

	if (has) {
		result = result.has(has)
	}

	if (ordered) {
		result = result.ordered(ordered)
	}

	if (sparse) {
		result = result.sparse()
	}

	// Fixed Length
	if (length) {
		result = result.length(length)
	}
	else {
		// Minimum Length
		if (min) {
			result = result.min(min)
		}

		// Maximum Length
		if (max) {
			result = result.max(max)
		}
	}

	// Required and Default
	result = req ? result.required() : result.allow('', null).empty(['', null]).default(def)

	// Returning
	return result
}

// Exporting
module.exports = {
	Type
}